[![pipeline status](https://gitlab.com/open-source-devex/terraform-modules/aws/alb-tg/badges/master/pipeline.svg)](https://gitlab.com/open-source-devex/terraform-modules/aws/alb-tg/commits/master)

# ECS Service with ALB target group

Terraform module for creating an ECS service with an associated ALB target group.
The module also adds a rule to the selected ALB listener and creates a DNS A record for the target group.
