output "alb_target_group_arns" {
  value = "${aws_alb_target_group.default.*.arn}"
}

output "fqdn" {
  value = "${local.inbound_domain_name}"
}
