terraform {
  required_version = ">= 0.10.13" # introduction of Local Values configuration language feature
}

# #################################################################
# Locals
# #################################################################
locals {
  common_tags = {
    Project     = "${var.project}"
    Environment = "${var.environment}"
  }

  # Workaround for interpolation not being able to "short-circuit" the evaluation of the conditional branch that doesn't end up being used
  # Source: https://github.com/hashicorp/terraform/issues/11566#issuecomment-289417805
  inbound_domain_name = "${split(",", var.alb_listener_rule_use_hostname ? join(",", aws_route53_record.hostname_service_record.*.fqdn) : "${var.dns_name}/")}"
}

resource "aws_alb_listener_rule" "default" {
  count = "${var.configure_listener_rule ? length(var.services) : 0}"

  listener_arn = "${var.alb_listener_arn}"
  priority     = "${lookup(var.alb_listener_rule_priority_map, lookup(var.services[count.index], "service_name"), 5)}"

  action {
    type             = "forward"
    target_group_arn = "${element(aws_alb_target_group.default.*.arn, count.index)}"
  }

  condition {
    field  = "${var.alb_listener_rule_use_hostname ? "host-header" : "path-pattern"}"
    values = ["${var.alb_listener_rule_use_hostname ? "${element(local.inbound_domain_name, count.index)}" : "/${lookup(var.services[count.index], "service_name")}/*"}"]
  }
}

resource "aws_alb_target_group" "default" {
  count = "${length(var.services)}"

  name     = "${var.environment}${title(lookup(var.services[count.index], "service_name"))}"
  port     = "${var.target_group_port}"
  protocol = "${var.target_group_protocol}"
  vpc_id   = "${var.vpc_id}"

  deregistration_delay = "${var.deregistration_delay}"

  stickiness {
    type            = "lb_cookie"
    cookie_duration = "${var.stickiness_cookie_duration}"
    enabled         = "${var.enable_stickiness}"
  }

  health_check {
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    interval            = "${var.health_check_interval}"
    timeout             = "${var.health_check_timeout}"
    path                = "${var.health_check_path_http_target}"
    port                = "${var.health_check_port_http_target}"
    matcher             = "${var.health_check_matcher_http_target}"
  }

  tags = "${merge(
    local.common_tags,
    map(
      "Name", "${title(var.project)} ${title(var.environment)} ${title(lookup(var.services[count.index], "service_name"))} Target Group"
    )
  )}"
}

resource "aws_route53_record" "hostname_service_record" {
  count = "${var.alb_listener_rule_use_hostname ? length(var.services) : 0}"

  zone_id = "${var.dns_zone_id}"
  name    = "${lower(lookup(var.services[count.index], "service_name"))}.${var.dns_name}"
  type    = "A"

  alias {
    name                   = "${lower(var.alb_dns_name)}"
    zone_id                = "${var.alb_dns_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_security_group_rule" "alb_to_targets" {
  description = "to ALB Targets"

  type                     = "egress"
  from_port                = "${var.target_group_port}"
  to_port                  = "${var.target_group_port}"
  protocol                 = "tcp"
  source_security_group_id = "${var.backend_security_group_id}"

  security_group_id = "${var.alb_security_group_id}"

  count = "${var.configure_backend_security_group ? 1 : 0}"
}

resource "aws_security_group_rule" "targets_from_from" {
  description = "from ALB"

  type                     = "ingress"
  from_port                = "${var.target_group_port}"
  to_port                  = "${var.target_group_port}"
  protocol                 = "tcp"
  source_security_group_id = "${var.alb_security_group_id}"

  security_group_id = "${var.backend_security_group_id}"

  count = "${var.configure_backend_security_group ? 1 : 0}"
}
