#!/usr/bin/env sh

set -e

if [ -f ".env" ]; then
  source .env
fi

set -v

CI_VARIABLES=ci-variables.tfvars
OVERRIDES_FILE=ci-overrides.tf
echo '
vpc_id = "string"
services = [{service_name = "string", target_group_name = "string"}]
alb_listener_arn = "string"
alb_listener_rule_priority = 1
alb_listener_rule_priority_map = {}
services = []
dns_zone_id = "string"
' > ${CI_VARIABLES}


echo 'provider "aws" {
  region     = "string"
  access_key = "string"
  secret_key = "string"
}
' >  ${OVERRIDES_FILE}

terraform init
terraform validate -var-file ${CI_VARIABLES} .

rm -f ci-*.tf*
