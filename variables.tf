variable "vpc_id" {
  description = "The ID of the VPC where the cluster instances will be created"
}

variable "project" {
  description = "Project name"
  default     = "project"
}

variable "environment" {
  description = "Environment name"
  default     = "env"
}

variable "alb_name" {
  description = "The name of the ALB. Default: alb"
  default     = "alb"
}

variable "services" {
  description = "List of maps with service parameters: 'service_name', 'target_group_name'"
  type        = "list"
}

variable "dns_zone_id" {
  description = "The ID of the DNS zone where to create new records for host based routing. Required when alb_listener_rule_use_hostname = true."
  default     = ""
}

variable "dns_name" {
  description = "The name of the DNS zone where to create new records for host based routing. Required when alb_listener_rule_use_hostname = true."
  default     = ""
}

variable "configure_listener_rule" {
  description = "Set to false to not add a ALB listener rule, which will mean the service gets no traffic from the ALB. Default: true"
  default     = true
}

variable "configure_backend_security_group" {
  description = "Set to true to configure security groups (of ALB and targets) to allow traffic between them. Requires alb_security_group_id and backend_security_group_id when set to true."
  default     = false
}

variable "alb_dns_zone_id" {
  description = "The ID of the DNS zone where the ALB domain name lives"
  default     = ""
}

variable "alb_dns_name" {
  description = "The domain name of the ALB."
  default     = ""
}

variable "alb_listener_arn" {
  description = "The ARN of the ALB listener where to add a rule for the service. It should typically be the HTTPS listener."
}

variable "alb_listener_rule_priority_map" {
  description = "The priority map for the service rule added to the ALB listener."
  type        = "map"
}

variable "alb_listener_rule_use_hostname" {
  description = "Set to true to use 'host-header' condition with generated domain name value. Set to false to use 'path-pattern' with service name as context path."
  default     = false
}

variable "alb_security_group_id" {
  description = "The ID of the ALB security group. Required in conjunction with configure_backend_security_group"
  default     = ""
}

variable "backend_security_group_id" {
  description = "The ID of the backend targets security group. Required in conjunction with configure_backend_security_group"
  default     = ""
}

variable "target_group_port" {
  description = "The port where the service target group will expect traffic. Default: 80."
  default     = 80
}

variable "target_group_protocol" {
  description = "The protocol in which the service target group will expect traffic. Default: HTTP."
  default     = "HTTP"
}

variable "health_check_healthy_threshold" {
  description = "How many consecutive health checks before considering the service healthy from the perspective of the ALB. Default: 2."
  default     = 2
}

variable "health_check_unhealthy_threshold" {
  description = "How many consecutive health checks before considering the service unhealthy from the perspective of the ALB. Default: 2."
  default     = 2
}

variable "health_check_path_http_target" {
  description = "The path used for reaching the health check endpoint. Default: /actuator/health."
  default     = "/actuator/health"
}

variable "health_check_port_http_target" {
  description = "The port used for reaching the health check endpoint. Default: traffic-port."
  default     = "traffic-port"
}

variable "health_check_matcher_http_target" {
  description = "The HTTP code to be expected from an healthy service. Default: 200."
  default     = "200"
}

variable "health_check_interval" {
  description = "How many secconds to wait between health checks. Default: 10."
  default     = 10
}

variable "health_check_timeout" {
  description = "How many secconds to wait before failing health check. Default: 5."
  default     = 5
}

variable "deregistration_delay" {
  description = "How many secconds to wait when deregistering services from the target group. Default: 5."
  default     = 5
}

variable "enable_stickiness" {
  description = "Set to false to disable stickiness at the ALB level. Default: true."
  default     = true
}

variable "stickiness_cookie_duration" {
  description = "How many seconds is a sticky session cookie valid for. Default: 86400 (1 day)."
  default     = 86400
}
